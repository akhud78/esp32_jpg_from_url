# esp32_jpg_from_url
- Test Wi-Fi connection 
- Test SD card
- Returns a JPEG image from a URL


## Requirements
- Ubuntu 20.04 LTS
- [ESP-IDF v5.0.2](https://docs.espressif.com/projects/esp-idf/en/v5.0.2/esp32s3/index.html)

## Building
- Get ESP-IDF
```
$ mkdir -p ~/esp
$ cd ~/esp
$ git clone -b v5.0.2 --recursive https://github.com/espressif/esp-idf.git esp-idf-v5.0.2
$ cd esp-idf-v5.0.2/
$ ./install.sh
```
- Checking
```
$ . $HOME/esp/esp-idf-v5.0.2/export.sh
$ idf.py --version
ESP-IDF v5.0.2
```
- Clone and setup [project](https://gitlab.com/akhud78/esp32_jpg_from_url)

```
$ cd ~/esp
$ git clone https://gitlab.com/akhud78/esp32_jpg_from_url
$ cd ~/esp/esp32_jpg_from_url
$ git submodule update --init --recursive
```
- Configuration
```
$ cd test
$ idf.py set-target esp32s3
$ idf.py menuconfig
```
- [Storage](components/storage/README.md)
- [Wi-Fi](components/wifi/README.md) - set ssid and password
- [HTTP Client](components/http_client/README.md)

## Usage

```
$ cd ~/esp/esp32_jpg_from_url/test/
$ rm build/CMakeCache.txt
$ idf.py -p /dev/ttyACM0 flash monitor
```
### Interactive test menu
- Press ENTER to see the list of tests.
```
Here's the test menu, pick your combo:
(1)	"get" [client]
(2)	"image reader" [client]
(3)	"spiffs mount" [ffs]
(4)	"spiffs format - all your data will be lost!" [format]
(5)	"spiffs write read remove" [ffs]
(6)	"fat mount" [sdcard]
...
```
